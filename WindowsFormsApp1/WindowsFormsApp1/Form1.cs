﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        int FirstNumber, SecondNumber, sum;

        private void btn_quit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btn_Subtract_Click(object sender, EventArgs e)
        {
            FirstNumber = int.Parse(txt_FirstNumber.Text);
            SecondNumber = int.Parse(txt_SecondNumber.Text);
            sum = FirstNumber - SecondNumber;
            MessageBox.Show("The subtraction of two numbers is " + sum.ToString());
        }

        private void btn_divide_Click(object sender, EventArgs e)
        {
            FirstNumber = int.Parse(txt_FirstNumber.Text);
            SecondNumber = int.Parse(txt_SecondNumber.Text);
            sum = FirstNumber / SecondNumber;
            MessageBox.Show("The divide of two numbers is " + sum.ToString());
        }

        private void btn_multiply_Click(object sender, EventArgs e)
        {
            FirstNumber = int.Parse(txt_FirstNumber.Text);
            SecondNumber = int.Parse(txt_SecondNumber.Text);
            sum = FirstNumber * SecondNumber;
            MessageBox.Show("The Multiplication of two numbers is " + sum.ToString());
        }

        public Form1()
        {
            InitializeComponent();
        }

        private void btn_Add_Click(object sender, EventArgs e)
        {
            FirstNumber = int.Parse(txt_FirstNumber.Text);
            SecondNumber = int.Parse(txt_SecondNumber.Text);
            sum = FirstNumber + SecondNumber;
            MessageBox.Show("The sum of two numbers is " + sum.ToString());
        }
    }
}
