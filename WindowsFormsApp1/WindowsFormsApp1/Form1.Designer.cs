﻿namespace WindowsFormsApp1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_FirstNumber = new System.Windows.Forms.Label();
            this.lbl_SecondNumber = new System.Windows.Forms.Label();
            this.btn_Add = new System.Windows.Forms.Button();
            this.btn_Subtract = new System.Windows.Forms.Button();
            this.btn_divide = new System.Windows.Forms.Button();
            this.btn_multiply = new System.Windows.Forms.Button();
            this.btn_quit = new System.Windows.Forms.Button();
            this.txt_FirstNumber = new System.Windows.Forms.TextBox();
            this.txt_SecondNumber = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // lbl_FirstNumber
            // 
            this.lbl_FirstNumber.AutoSize = true;
            this.lbl_FirstNumber.Location = new System.Drawing.Point(75, 89);
            this.lbl_FirstNumber.Name = "lbl_FirstNumber";
            this.lbl_FirstNumber.Size = new System.Drawing.Size(66, 13);
            this.lbl_FirstNumber.TabIndex = 0;
            this.lbl_FirstNumber.Text = "First Number";
            // 
            // lbl_SecondNumber
            // 
            this.lbl_SecondNumber.AutoSize = true;
            this.lbl_SecondNumber.Location = new System.Drawing.Point(75, 148);
            this.lbl_SecondNumber.Name = "lbl_SecondNumber";
            this.lbl_SecondNumber.Size = new System.Drawing.Size(84, 13);
            this.lbl_SecondNumber.TabIndex = 1;
            this.lbl_SecondNumber.Text = "Second Number";
            // 
            // btn_Add
            // 
            this.btn_Add.Location = new System.Drawing.Point(66, 328);
            this.btn_Add.Name = "btn_Add";
            this.btn_Add.Size = new System.Drawing.Size(75, 23);
            this.btn_Add.TabIndex = 2;
            this.btn_Add.Text = "Add";
            this.btn_Add.UseVisualStyleBackColor = true;
            this.btn_Add.Click += new System.EventHandler(this.btn_Add_Click);
            // 
            // btn_Subtract
            // 
            this.btn_Subtract.Location = new System.Drawing.Point(189, 328);
            this.btn_Subtract.Name = "btn_Subtract";
            this.btn_Subtract.Size = new System.Drawing.Size(75, 23);
            this.btn_Subtract.TabIndex = 3;
            this.btn_Subtract.Text = "Subtract";
            this.btn_Subtract.UseVisualStyleBackColor = true;
            this.btn_Subtract.Click += new System.EventHandler(this.btn_Subtract_Click);
            // 
            // btn_divide
            // 
            this.btn_divide.Location = new System.Drawing.Point(330, 328);
            this.btn_divide.Name = "btn_divide";
            this.btn_divide.Size = new System.Drawing.Size(75, 23);
            this.btn_divide.TabIndex = 4;
            this.btn_divide.Text = "Divide";
            this.btn_divide.UseVisualStyleBackColor = true;
            this.btn_divide.Click += new System.EventHandler(this.btn_divide_Click);
            // 
            // btn_multiply
            // 
            this.btn_multiply.Location = new System.Drawing.Point(450, 328);
            this.btn_multiply.Name = "btn_multiply";
            this.btn_multiply.Size = new System.Drawing.Size(75, 23);
            this.btn_multiply.TabIndex = 5;
            this.btn_multiply.Text = "Multiply";
            this.btn_multiply.UseVisualStyleBackColor = true;
            this.btn_multiply.Click += new System.EventHandler(this.btn_multiply_Click);
            // 
            // btn_quit
            // 
            this.btn_quit.Location = new System.Drawing.Point(586, 328);
            this.btn_quit.Name = "btn_quit";
            this.btn_quit.Size = new System.Drawing.Size(75, 23);
            this.btn_quit.TabIndex = 6;
            this.btn_quit.Text = "Quit";
            this.btn_quit.UseVisualStyleBackColor = true;
            this.btn_quit.Click += new System.EventHandler(this.btn_quit_Click);
            // 
            // txt_FirstNumber
            // 
            this.txt_FirstNumber.Location = new System.Drawing.Point(305, 89);
            this.txt_FirstNumber.Name = "txt_FirstNumber";
            this.txt_FirstNumber.Size = new System.Drawing.Size(100, 20);
            this.txt_FirstNumber.TabIndex = 7;
            // 
            // txt_SecondNumber
            // 
            this.txt_SecondNumber.Location = new System.Drawing.Point(305, 148);
            this.txt_SecondNumber.Name = "txt_SecondNumber";
            this.txt_SecondNumber.Size = new System.Drawing.Size(100, 20);
            this.txt_SecondNumber.TabIndex = 8;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.txt_SecondNumber);
            this.Controls.Add(this.txt_FirstNumber);
            this.Controls.Add(this.btn_quit);
            this.Controls.Add(this.btn_multiply);
            this.Controls.Add(this.btn_divide);
            this.Controls.Add(this.btn_Subtract);
            this.Controls.Add(this.btn_Add);
            this.Controls.Add(this.lbl_SecondNumber);
            this.Controls.Add(this.lbl_FirstNumber);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_FirstNumber;
        private System.Windows.Forms.Label lbl_SecondNumber;
        private System.Windows.Forms.Button btn_Add;
        private System.Windows.Forms.Button btn_Subtract;
        private System.Windows.Forms.Button btn_divide;
        private System.Windows.Forms.Button btn_multiply;
        private System.Windows.Forms.Button btn_quit;
        private System.Windows.Forms.TextBox txt_FirstNumber;
        private System.Windows.Forms.TextBox txt_SecondNumber;
    }
}

